var fs = require("fs");  
 
function modifytext(text){ 
    text = text.replace(/\r?\n|\r/g, ""); 
    text = text.replace(/\s/g, '')
    text = text.split('@n').join('\n');
    text = text.split('@s').join(' ');
     
    return text; 
} 
 
function readfile(){ 
    fs.readFile("minified.txt", function(err, buf) { 
   
        modified = modifytext(buf.toString()) 
        writefile(modified); 
        }); 
} 
 
function writefile(text){ 
    fs.writeFile("maxified.txt", text, (err) => { 
      if (err) console.log(err); 
      console.log("Successfully Written to File."); 
    }); 
} 
 
 
readfile() 
 
