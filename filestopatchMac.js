var fs = require("fs");
var mkdirp = require('mkdirp');
var path = require('path'); 
const shell = require('shelljs')
var exec = require('child_process').exec;
var os = require("os")


var sourcedirectory = "/Users/srowlo/netcore/algomachine/v4/"
var destinationdirectory="/Users/apple/Documents/CodeProjects/algomachine/v5/"


var pathstoremovefrompatch="/Users/srowlo/netcore/algomachine/v4"
var nongitfolder="/Users/apple/patchwork";

//optional
var allowedextensions = ".cs,.txt"
var excludedfilesordir="bin,obj,ContactOutput.txt,alldiff.patch"
//user inputs end here


var correctedpatchfile="corrected_alldiff.patch"


//auto generated

var option=undefined

if(process.argv.length>0)
{
    option = process.argv[2]
    
}

var Minoutputdirectory= "MinOutputs"
var Maxoutputdirectory= "MaxOutputs"

var patchfile="alldiff.patch"

var minifiedfile="minified.txt"

var gitapplysh="gitapply.sh"
var gitpatchsh = "gitpatch.sh"

function copydestinationfiles(){
    console.log('copied source')
    fs.readdir(destinationdirectory, function(err, filenames) {
        if (err) {
          onError(err);
          return;
        }
  
        var allext= allowedextensions.split(",")
        var excluded = excludedfilesordir.split(",")
          
        filenames.forEach(filename => {
  
          var extName = path.extname(filename);
          if (allext.indexOf(extName)==-1)
          return;
  
          if(excluded.indexOf(filename)!=-1)
          return;
  
          var currentfile = destinationdirectory+filename;
          // console.log(currentfile);
          
  
          fs.copyFile(currentfile,nongitfolder+"/"+filename,function(err){
              if (err) console.log(err);

            });
        });
          
  
        
      });
}

function copypatchfileandsh(){
    fs.copyFile(Maxoutputdirectory+"/"+correctedpatchfile,nongitfolder+"/"+correctedpatchfile,function(err){
        if (err) console.log(err);
        
      });
      fs.copyFile(__dirname+"/"+gitapplysh,nongitfolder+"/"+gitapplysh,function(err){
        if (err) console.log(err);
        
      });
}

function correctpatchfile(){
    var genpathpath = Maxoutputdirectory+"/"+patchfile;
    function modifypatch(text){ 
        upaths = pathstoremovefrompatch.split(",")
        upaths.forEach(element => {
            
            var re = new RegExp(element,"g");
            text = text.replace(re, ""); 
        });
        
        return text; 
    } 
     
    function readfile(patchfile){ 
        fs.readFile(patchfile, function(err, buf) { 
       
            modified = modifypatch(buf.toString()) 
            writefile(modified); 
            }); 
    } 
     
    function writefile(text){ 
        mkdirp(Maxoutputdirectory);
        fs.writeFile(Maxoutputdirectory+"/"+correctedpatchfile, text, (err) => { 
          if (err) console.log(err); 
          console.log("Successfully Written to File."); 
          
        }); 
    } 
     
     
    readfile(genpathpath) 
}

function runlastpatch(){
    var cmd = "cd "+nongitfolder+" \n pwd \n sh "+gitapplysh+" "+correctedpatchfile
    console.log("====")
    console.log(cmd)
    // exec(cmd,{cwd:nongitfolder });
    shell.exec(cmd)
    // exec('git status', {cwd: '/home/ubuntu/distro'}, /* ... */);
    setTimeout(() => {
        exec('open '+nongitfolder);
    }, 2000);

}

function gitApply(filename){
    correctpatchfile()
    mkdirp(nongitfolder)
    copydestinationfiles();
    copypatchfileandsh();
    setTimeout(() => {
        runlastpatch();    
    }, 3000);
    
    // shell.exec("cd "+sourcedirectory+"\n echo test >>testing.txt")
    // console.log('patch gen requests here'+filename)
    // shell.exec('sh gitapply.sh '+filename)
}

function maxifyPatch(){
    var minpath = Minoutputdirectory+"/"+minifiedfile;
    function modifymin(text){ 
        text = text.replace(/\r?\n|\r/g, ""); 
        text = text.split('@n').join('\n');
        text = text.split('@s').join(' ');
         
        return text; 
    } 
     
    function readfile(minfile){ 
        fs.readFile(minfile, function(err, buf) { 
       
            modified = modifymin(buf.toString()) 
            writefile(modified); 
            }); 
    } 
     
    function writefile(text){ 
        mkdirp(Maxoutputdirectory);
        fs.writeFile(Maxoutputdirectory+"/"+patchfile, text, (err) => { 
          if (err) console.log(err); 
          console.log("Successfully Written to File."); 
          gitApply(Maxoutputdirectory+"/"+patchfile)
        }); 
    } 
     
     
    readfile(minpath) 
    
     
}

function minifyPatch(){
    var patchpath = Minoutputdirectory+"/"+patchfile;
    function modifypatch(text){
        
        if(text.indexOf("@n")>0||text.indexOf("@s")>0) 
        console.log('it has special chars, please correct') 
        text = text.replace(/\t/g, "@s@s@s@s"); 
        text = text.replace(/\r?\n|\r/g, "@n"); 
        text= text.split(' ').join('@s') 
        return text;
    }
    
    function readAndModifyPatch(pfile){
        fs.readFile(pfile, function(err, buf) {
      
            modified = modifypatch(buf.toString())
            writepatch(modified);
            });
    }
    
    function writepatch(text){
        fs.writeFile(Minoutputdirectory+"/"+minifiedfile, text, (err) => {
        if (err) console.log(err);
          console.log("Successfully Written to File.");
        });
    }

    readAndModifyPatch(patchpath);

}

function clearPatch(){
    mkdirp(Minoutputdirectory)
    fs.writeFile(Minoutputdirectory+"/"+patchfile, "", (err) => {
      if (err) console.log(err);
    });
}

function modifytext(text){
    
    text = text.replace(/[\r\n]+/g, '\n');
    return text;
}

function genPatch(filename){
console.log(filename)
    //destination check
    var destpath = destinationdirectory+filename;
    if (!fs.existsSync(destpath)) {
        destpath="/dev/null"
      }
      
    //sourcepath
    var sourcepath = sourcedirectory+filename;

    // console.log('patch gen requests here'+filename)
    var cmd = 'sh '+gitpatchsh+' '+destpath+" "+sourcepath+" "+Minoutputdirectory+"/"+patchfile
    console.log(cmd)
    shell.exec(cmd)
}

function writefile(text,filename){
    mkdirp(Minoutputdirectory)
    fs.writeFile(Minoutputdirectory+"/"+filename, text, (err) => {
      if (err) console.log(err);
    //   console.log("Successfully Written to File.");
    //once files are ready, start generating patch file
    genPatch(filename)
    });
}


function readFiles(dirname, onError) {
    fs.readdir(dirname, function(err, filenames) {
      if (err) {
        onError(err);
        return;
      }

      var allext= allowedextensions.split(",")
      var excluded = excludedfilesordir.split(",")
        
      filenames.forEach(filename => {

        var extName = path.extname(filename);
        if (allext.indexOf(extName)==-1)
        return;

        if(excluded.indexOf(filename)!=-1)
        return;

        var currentfile = dirname+filename;
        // console.log(currentfile);
        

        fs.readFile(currentfile, function(err, buf) {
            
            if(buf==undefined){
                console.log(currentfile)
            }
            
            filetext = modifytext(buf.toString())
            //removed extra lines and saved to local genoutput directory
            writefile(filetext,filename)
            
            // totaltext = totaltext+"\n"+ "@@filename "+currentfile+"\n"+ filetext;
            
            });  
      });
        

      
    });
  }

  function startexecution(){
    if(option=="min"){
        clearPatch();
        readFiles(sourcedirectory,function(err){console.log(err)});
        setTimeout(() => {
              minifyPatch()
        }, 5000);
    }
    else if (option=="max"){
        maxifyPatch();
    }
    else{
        console.log("please provide a valid arguement: ex: node Z_gensingle.js min")
    }
  }

if(os.platform().indexOf("win")!=-1 && os.platform().indexOf("darwin")==-1)
{
    console.log('this app is not configured to run with windows yet')
}
else{
    startexecution();
}


  
  
  