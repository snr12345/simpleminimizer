var fs = require("fs");
var mkdirp = require('mkdirp');
var path = require('path'); 
const shell = require('shelljs')


var sourcedirectory = "/Users/apple/Documents/CodeProjects/algomachine/v2/"
var destinationdirectory="/Users/apple/Documents/CodeProjects/algomachine/v4/"

//optional
var allowedextensions = ".cs,.txt"
var excludedfilesordir="bin,obj,ContactOutput.txt,alldiff.patch"

//auto generated

var option=undefined

if(process.argv.length>0)
{
    option = process.argv[2]
    
}

var outputdirectory= "MinOutputs"

var patchfile="alldiff.patch"
var minifiedfile="minified.txt"

function maxifyPatch(){
    
}

function minifyPatch(){
    var patchpath = outputdirectory+"/"+patchfile;
    function modifypatch(text){
        
        text = text.replace(/\r?\n|\r/g, "");
        text= text.split(' ').join(' ')
        return text;
    }
    
    function readAndModifyPatch(pfile){
        fs.readFile(pfile, function(err, buf) {
      
            modified = modifypatch(buf.toString())
            writepatch(modified);
            });
    }
    
    function writepatch(text){
        fs.writeFile(outputdirectory+"/"+minifiedfile, text, (err) => {
        if (err) console.log(err);
          console.log("Successfully Written to File.");
        });
    }

    readAndModifyPatch(patchpath);

}

function clearPatch(){
    mkdirp(outputdirectory)
    fs.writeFile(outputdirectory+"/"+patchfile, "", (err) => {
      if (err) console.log(err);
    });
}

function modifytext(text){
    
    text = text.replace(/[\r\n]+/g, '\n');
    return text;
}

function genPatch(filename){

    //destination check
    var destpath = destinationdirectory+filename;
    if (!fs.existsSync(destpath)) {
        destpath="/dev/null"
      }
      
    //sourcepath
    var sourcepath = sourcedirectory+filename;

    // console.log('patch gen requests here'+filename)
    shell.exec('sh genpatch.sh '+destpath+" "+sourcepath+" "+outputdirectory+"/"+patchfile)
}

function writefile(text,filename){
    mkdirp(outputdirectory)
    fs.writeFile(outputdirectory+"/"+filename, text, (err) => {
      if (err) console.log(err);
    //   console.log("Successfully Written to File.");
    //once files are ready, start generating patch file
    genPatch(filename)
    });
}


function readFiles(dirname, onError) {
    fs.readdir(dirname, function(err, filenames) {
      if (err) {
        o

nError(err);
        return;
      }

      var allext= allowedextensions.split(",")
      var excluded = excludedfilesordir.split(",")
        
      filenames.forEach(filename => {

        var extName = path.extname(filename);
        if (allext.indexOf(extName)==-1)
        return;

        if(excluded.indexOf(filename)!=-1)
        return;

        var currentfile = dirname+filename;
        // console.log(currentfile);
        

        fs.readFile(currentfile, function(err, buf) {
            
            if(buf==undefined){
                console.log(currentfile)
            }
            
            filetext = modifytext(buf.toString())
            //removed extra lines and saved to local genoutput directory
            writefile(filetext,filename)
            
            // totaltext = totaltext+"\n"+ "@@filename "+currentfile+"\n"+ filetext;
            
            });  
      });
        

      
    });
  }

if(option=="min"){
    clearPatch();
    readFiles(sourcedirectory,function(err){console.log(err)});
    setTimeout(() => {
          minifyPatch()
    }, 5000);
}
else if (option=="max"){
    maxifyPatch();
}
else{
    console.log("please provide a valid arguement: ex: node Z_gensingle.js min")
}
  
  
  