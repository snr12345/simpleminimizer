var fs = require("fs");  
 
function modifytext(text){ 
    if(text.indexOf("@n")>0||text.indexOf("@s")>0) 
    console.log('it has special chars, please correct') 
    text = text.replace(/\t/g, "@s@s@s@s"); 
    text = text.replace(/\r?\n|\r/g, "@n"); 
    text= text.split(' ').join('@s') 
    return text; 
} 
 
function readfile(){ 
    fs.readFile("maxified.txt", function(err, buf) { 
   
        modified = modifytext(buf.toString()) 
        writefile(modified); 
        }); 
} 
 
function writefile(text){ 
    fs.writeFile("minified.txt", text, (err) => { 
      if (err) console.log(err); 
      console.log("Successfully Written to File."); 
    }); 
} 
 
 
readfile() 
 
